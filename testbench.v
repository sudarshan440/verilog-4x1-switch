module tb;
  reg clk,rst;
     tx_4x1_top u2 (clk,rst);
  
  
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars(0,tb);
    //read_en = 0;
    //read_en1=0;read_en0=0;write_en =0;
    clk =0; 
    #2;
    rst = 1;
    #4;
    rst = 0; #1;
    //read_en=1;read_en0=1;read_en1=1;write_en=1;
    #500;
    
  end
  always #1 clk = ~clk;
  
endmodule 