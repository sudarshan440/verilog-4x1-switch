module rx (clk,rst,data_out,valid_out,busy_in);
input clk,rst,valid_out;
output reg busy_in;
  input [7:0] data_out;

  reg [9:0] address;
  reg [6:0] counter;
  reg busy_assert_start,state;
  reg [31:0] data_memory[0:1023];

//for datat latching on rising edge
always @(posedge clk)
begin
  if (valid_out == 1) begin
       if (address < 64) begin
         data_memory[address] <= data_out;
			address <= address + 1;
		end //lower if;
	    else
		address <= 0;
        end	//upper if	
	
end
	

	//for busy signal
always @ (posedge clk)
begin
  if (rst)
    begin
      state <= 0; busy_in <= 1;
    end
  else
    begin
      case (state)
        0 : if (busy_assert_start == 1) begin
          state <= 1; busy_in <= 1;end
        1:   if (address < 64)
		         busy_in <= 1;
	        else begin
              busy_in <= 0; state <=0; end
      endcase
    end // else loop 

end //always
  
        always @ (posedge clk) // delay until data is written onto memory of repater module.
begin
  if (counter < 67) begin
		counter <= counter + 1;
    busy_assert_start <= 0; 
     end
	else begin
      counter <= 0;busy_assert_start<=1; end
	

end //always

endmodule