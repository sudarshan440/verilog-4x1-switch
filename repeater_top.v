// Code your design here
// Code your design here
module repeater_top(clk,rst,valid_in,data_in,busy_out,valid_out,data_out,busy_in);
input clk,rst,valid_in,busy_in;
input [7:0] data_in;
output reg busy_out;
output reg valid_out;
  output reg [7:0] data_out;  
  reg [5:0] address; reg [0:5] count;
  reg [31:0] data_memory[0:1023];
  reg [7:0] counter_busy_assert;
  reg busy_assert_start,state_4_busy_assert;
reg state;
always @(posedge clk) //FOR LATCHING DATA
begin
	 if (valid_in == 1) begin
       if (address < 64) begin
			data_memory[address] <= data_in;
			address <= address + 1;
		end //lower if;
       else
         address <= 0;
	            end	//upper if	
end //always


  
  //for busy signal
  always @ (posedge clk) // delay until reset is low
begin
  if (rst) begin counter_busy_assert <= 0; end
  else begin
    if (counter_busy_assert > 61) begin
		  counter_busy_assert <= 0;
    		busy_assert_start <= 0; 
    		 end
		else begin 	counter_busy_assert <= counter_busy_assert + 1;busy_assert_start<=1; end
  end //else

end //always
  
  
  always @ (posedge clk)
begin
  if (rst)
    begin
      state_4_busy_assert <= 0; busy_out <= 1;
    end
  else
    begin
      case (state_4_busy_assert)
        0 : if (busy_assert_start == 1) begin
            state_4_busy_assert <= 1; 
            busy_out <= 0;end
        1:   if (address == 63) begin  busy_out <= 1; state_4_busy_assert <= 0; end
	        else
		        busy_out <= 0;
      endcase
    end // else loop 

end //always


//for repeater tx module
//assign valid_out = ~ bust_out;
//tx u2 (clk,rst,data_memory[address],valid_out,busy_in);

always  @(posedge clk)
begin
 if (rst) begin	state <= 0; 
 end
 else 
    begin	
  	 case (state)
	 	
	    0:
          if (busy_in == 0)
            begin
	        	valid_out <= 1;
		        state <= 1; end
	    1: 
          if (count == 63) 
            begin
		     valid_out <= 0; state <= 0;
           end
	endcase
end //else loop
end //always	 		


  
  //data_in block

always  @(posedge clk)
begin
  if (valid_out ==1)
	begin
         data_out <= data_memory[count];  
	   end //for else
		
  end 				
		  
//count block	
always  @(posedge clk)
begin
  if (rst ==1) begin
    count <= 0; end
  if (valid_out == 1) begin   count <= count + 1;  end
    if (count == 64) 
        count <= 0;
  	
		
end  //always 				
  

endmodule




 