module tx_4x1_top(clk,rst);
input clk,rst;
wire [7:0] data_in;
wire [7:0] data_out;
wire busy_in;
wire busy_out;
wire valid_in;
wire  valid_out;
//wire [7:0] data_memory[0:63];
  reg [7:0] imem [0:64];
integer i;
// block for memory
   
  
  //module tx(clk,rst,data_in,valid,busy)

tx4x1 tx4x1_dut(clk,rst,data_in,valid_in,busy_out); //tx module

repeater_top u1(clk,rst,valid_in,data_in,busy_out,valid_out,data_out,busy_in); //repeater module

rx rx_dut(clk,rst,data_out,valid_out,busy_in); // rx module 



endmodule
